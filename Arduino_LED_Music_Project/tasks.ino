#include "tasks.h"
#include "tasksCfg.h"

#define TASK_50MS_DELAY   50
#define TASK_100MS_DELAY  100
#define TASK_500MS_DELAY  500

#define MAX_SIZE_INT      32768

int prev50ms = 0;
int prev100ms = 0;
int prev500ms = 0;

void taskStartUp()
{
  for(int i = 0; i < NR_TASKS_STARTUP; i++)
  {
    taskListStartUp[i]();
  }
}

void taskScheduler()
{
  for(int i = 0; i < NR_TASKS_FAST; i++)
  {
    taskListFast[i]();
  }

  if(prev50ms > (millis() % MAX_SIZE_INT))
    prev50ms = 0;
  if(prev100ms > (millis() % MAX_SIZE_INT))
    prev100ms = 0;
  if(prev500ms > (millis() % MAX_SIZE_INT))
    prev500ms = 0;
  
  if((millis() % MAX_SIZE_INT) > prev50ms + TASK_50MS_DELAY)
  {
    prev50ms = (millis() % MAX_SIZE_INT);
    
    for(int i = 0; i < NR_TASKS_50ms; i++)
    {
      taskList50ms[i]();
    }
  }
  if((millis() % MAX_SIZE_INT) > prev100ms + TASK_100MS_DELAY)
  {
    prev100ms = (millis() % MAX_SIZE_INT);

    for(int i = 0; i < NR_TASKS_100ms; i++)
    {
      taskList100ms[i]();
    }
  }
  if((millis() % MAX_SIZE_INT) > prev500ms + TASK_500MS_DELAY)
  {
    prev500ms = (millis() % MAX_SIZE_INT);

    for(int i = 0; i < NR_TASKS_500ms; i++)
    {
      taskList500ms[i]();
    }
  }
}
