#include "bt.h"

#define BAUD_RATE_BT  9600
#define STOP_BYTE     (unsigned int) 0x7F
#define NR_STOP_BYTES (unsigned int) 4

enum btStatus {BT_IDLE, BT_RECEIVING, BT_RECEIVED_FINISH};

btStatus rxStatus = BT_IDLE;

byte rxCount = 0;
byte stopCount = 0;

void startUpBT()
{
  Serial3.begin(BAUD_RATE_BT);
}

void startReceptionBT()
{
  if(Serial3.available() && (BT_IDLE == rxStatus))
  {
    Serial.println("START REC");
    rxStatus = BT_RECEIVING;
    rxCount = 0;

    rxMessage[rxCount] = Serial3.read();

    Serial.print("rxMessage[0]: ");
    Serial.println(rxMessage[rxCount]);
  
    if(STOP_BYTE == rxMessage[rxCount])
    {
      stopCount++;
      if(NR_STOP_BYTES == stopCount)
      {
        rxStatus = BT_RECEIVED_FINISH;
      }
    }
    else
    {
      stopCount = 0;
    }

    rxCount++;
  }
}

void receiveBT()
{
  switch(rxStatus)
  {
    case BT_RECEIVING:
      if(Serial3.available())
      {
        rxMessage[rxCount] = Serial3.read();
        
        if(STOP_BYTE == rxMessage[rxCount])
        {
          stopCount++;
          if(NR_STOP_BYTES == stopCount)
          {
            rxStatus = BT_RECEIVED_FINISH;
          }
        }
        else
        {
          stopCount = 0;
        }
    
        rxCount++;
      }
      break;
      
    case BT_RECEIVED_FINISH:
      rxMessageLen = rxCount - 4;
      Serial.print("rxCount: ");
      Serial.println(rxCount);
      msgReceivedFlag = 1;

      for(int i = 0; i < rxMessageLen; i++)
      {
        Serial.print(rxMessage[i]);
        Serial.print(" ");
      }

      Serial.println();

      rxCount = 0;
      stopCount = 0;
      rxStatus = BT_IDLE;
      
      break;
  }
}

void sendBT(int len, byte msg[])
{
  
}
