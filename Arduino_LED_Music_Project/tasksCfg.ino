#include "tasksCfg.h"
#include "tasksImpl.h"

void (*taskListStartUp[NR_TASKS_STARTUP])() = { defaultInit };
void (*taskListFast[NR_TASKS_FAST])() = { taskFast };
void (*taskList50ms[NR_TASKS_50ms])() = { task50ms_1 };
void (*taskList100ms[NR_TASKS_100ms])() = { task100ms_1 };
void (*taskList500ms[NR_TASKS_500ms])() = { task500ms_1 };
