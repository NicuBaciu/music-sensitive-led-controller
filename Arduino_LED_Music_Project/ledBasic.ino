#include "ledBasic.h"

#define LED_PORT_RED    2
#define LED_PORT_GREEN  3
#define LED_PORT_BLUE   4

void initLEDs()
{
  pinMode(LED_PORT_RED, OUTPUT);
  pinMode(LED_PORT_GREEN, OUTPUT);
  pinMode(LED_PORT_BLUE, OUTPUT);
}

void displayColor()
{
  /*Serial.print("R: ");
  Serial.println(ledRValue);
  Serial.print("G: ");
  Serial.println(ledGValue);
  Serial.print("B: ");
  Serial.println(ledBValue);*/
  
  analogWrite(LED_PORT_RED, ledRValue);
  analogWrite(LED_PORT_GREEN, ledGValue);
  analogWrite(LED_PORT_BLUE, ledBValue);


  
  /*analogWrite(LED_PORT_RED, 0);
  analogWrite(LED_PORT_GREEN, 0);
  analogWrite(LED_PORT_BLUE, 255);*/
}
