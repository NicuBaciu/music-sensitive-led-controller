#include "msgHandler.h"
#include "bt.h"
#include "ledController.h"

#define TYPE_CH_MODE    0x10
#define TYPE_SET_COLOR  0x01

void dispatcher()
{
  if(msgReceivedFlag)
  {
    if(TYPE_CH_MODE == rxMessage[0])
    {
      Serial.println("CHANGE MODE");
      changeMode(rxMessage[1]);
      msgReceivedFlag = 0;
    }
    else if(TYPE_SET_COLOR == rxMessage[0])
    {
      Serial.println("SET COLOR");
      modeSetSimpleColor(rxMessage + 1);
      msgReceivedFlag = 0;
    }
  }
}

void modeSetSimpleColor(byte colorMsg[])
{
   controllerR = colorMsg[0];
   controllerG = colorMsg[1];
   controllerB = colorMsg[2];

   Serial.print("contRED: ");
   Serial.println(controllerR);
   Serial.print("contGREEN: ");
   Serial.println(controllerG);
   Serial.print("contBLUE: ");
   Serial.println(controllerB);
}

void changeMode(byte mode)
{
  ledControllerChangeMode(mode);
}
