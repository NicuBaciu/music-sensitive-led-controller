#ifndef _LED_CONTROLLER_H
#define _LED_CONTROLLER_H

enum modeType { MODE_IDLE, MODE_SIMPLE_COLOR, MODE_SWEEP, MODE_MUSIC };

modeType crtMode = MODE_MUSIC;
byte controllerR, controllerG, controllerB;

void processModes();
void ledControllerChangeMode(byte mode);

#endif // _LED_CONTROLLER_H 
