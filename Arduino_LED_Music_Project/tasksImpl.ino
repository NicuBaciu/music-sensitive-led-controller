#include "tasksImpl.h"
#include "bt.h"
#include "ledBasic.h"
#include "msgHandler.h"
#include "ledController.h"
#include "adcController.h"

void taskFast()
{
  readFilters();
}

void defaultInit()
{
  startUpBT();
  initLEDs();
}

void task50ms_1()
{
  processModes();
  displayColor();
}

void task100ms_1()
{
  receiveBT();
  dispatcher();
}

void task500ms_1()
{
  startReceptionBT();
}
