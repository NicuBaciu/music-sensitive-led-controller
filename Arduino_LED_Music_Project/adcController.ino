#include "adcController.h"

#include "avdweb_AnalogReadFast.h"
#include <RunningAverage.h>

#define bassTH  60
#define trebleTH  20
#define midTH  100

int bassTrigger = 0;
int midTrigger = 0;
int trebleTrigger = 0;

const byte bassPin = A0;
const byte midPin = A1; 
const byte treblePin = A2;

const int testsamples = 40;
const int ravgSamples = 16;

RunningAverage fast10RavgBass(ravgSamples);
RunningAverage fast10RavgTestBass(testsamples);

RunningAverage fast10RavgTreble(ravgSamples);
RunningAverage fast10RavgTestTreble(testsamples);

RunningAverage fast10RavgMid(ravgSamples);
RunningAverage fast10RavgTestMid(testsamples);

float areadV;

void readFilters()
{ 
  float value;
  
  for(int i = 0; i < testsamples; i++)
  {
    fast10RavgTreble.addValue(abs(analogReadFast(treblePin) - 400));
    trebleTrigger = fast10RavgTreble.getAverage();
    fast10RavgTestTreble.addValue(value);

    fast10RavgMid.addValue(abs(analogReadFast(midPin) - 400));
    midTrigger = fast10RavgMid.getAverage();
    fast10RavgTestMid.addValue(value);
    
    fast10RavgBass.addValue(abs(analogReadFast(bassPin) - 400));
    value = fast10RavgBass.getAverage();
    fast10RavgTestBass.addValue(value);
    
    if(value > bassTH)
    {
      bassTrigger = 1;
    }
    else
    {
      bassTrigger = 0;
    }
  }
}
