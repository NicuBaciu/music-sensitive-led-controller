#ifndef _TASKS_CFG_H
#define _TASKS_CFG_H

#define NR_TASKS_STARTUP  1

#define NR_TASKS_FAST     1
#define NR_TASKS_50ms     1
#define NR_TASKS_100ms    1
#define NR_TASKS_500ms    1

extern void (*taskListStartUp[NR_TASKS_STARTUP])();

extern void (*taskListFast[NR_TASKS_FAST])();
extern void (*taskList50ms[NR_TASKS_50ms])();
extern void (*taskList100ms[NR_TASKS_100ms])();
extern void (*taskList500ms[NR_TASKS_500ms])();

#endif // _TASKS_CFG_H
