#include "ledController.h"
#include "ledBasic.h"
#include "adcController.h"

#include "colorLUTS.h"

#define COLOR_INCREMENT 10

#define MIN_THRESHOLD   5
#define MAX_THRESHOLD   250

int dirR, dirG, dirB;

void ledControllerChangeMode(byte mode)
{
  crtMode = (modeType) mode;

  if(MODE_SWEEP == crtMode)
  {
    ledRValue = 254;
    ledGValue = 0;
    ledBValue = 0;

    dirR = 0;
    dirG = 0;
    dirB = 0;
  }
  Serial.print("crtMode: ");
  Serial.println(crtMode);
}

int colorCounter = 0;
int jumpTimeout = 0;

void setColor(int intensity, int colorType)
{ 
  if(colorType == 1) // TREBLE
  {
    ledRValue = colorLUT[colorCounter].r - intensity;
    ledGValue = colorLUT[colorCounter].g - intensity;
    ledBValue = colorLUT[colorCounter].b - intensity;

    if(ledRValue < 0)
      ledRValue = 0;
    if(ledGValue < 0)
      ledGValue = 0;
    if(ledBValue < 0)
      ledBValue = 0;
  }
  else if(colorType == 2)
  {
    if(jumpTimeout == 0)
    {
      colorCounter += 150;
      jumpTimeout = 7;
    }
    else
      colorCounter += 20;
  }
  else
  {
    ledRValue = colorLUT[colorCounter].r;
    ledGValue = colorLUT[colorCounter].g;
    ledBValue = colorLUT[colorCounter].b;
  }

  if(jumpTimeout > 1)
    jumpTimeout--;
  else
    jumpTimeout = 1;
  
  colorCounter += 2;
  if(colorCounter >= 762)
    colorCounter = 0;
}

int noBass = 1;
int bassTimeout = 5;

void processModes()
{ 
  if(MODE_SIMPLE_COLOR == crtMode)
  {
    if(controllerR <= MIN_THRESHOLD)
      ledRValue = 0;
    else if(controllerR >= MAX_THRESHOLD)
      ledRValue = 255;
    else
      ledRValue = controllerR;

    if(controllerG <= MIN_THRESHOLD)
      ledGValue = 0;
    else if(controllerG >= MAX_THRESHOLD)
      ledGValue = 255;
    else
      ledGValue = controllerG;

    if(controllerB <= MIN_THRESHOLD)
      ledBValue = 0;
    else if(controllerB >= MAX_THRESHOLD)
      ledBValue = 255;
    else
      ledBValue = controllerB;
  }

  if(MODE_SWEEP == crtMode)
  {
    ledRValue += dirR;
    ledGValue += dirG;
    ledBValue += dirB;
    
    if ((ledRValue >= MAX_THRESHOLD) && (ledGValue <= MIN_THRESHOLD) && (ledBValue <= MIN_THRESHOLD))
    {
      dirR = 0;
      dirG = COLOR_INCREMENT;
      dirB = 0;
    }
    if((ledRValue >= MAX_THRESHOLD) && (ledGValue >= MAX_THRESHOLD) && (ledBValue <= MIN_THRESHOLD))
    {
      dirR = -COLOR_INCREMENT;
      dirG = 0;
      dirB = 0;
    }
    if((ledRValue <= MIN_THRESHOLD) && (ledGValue >= MAX_THRESHOLD) && (ledBValue <= MIN_THRESHOLD))
    {
      dirR = 0;
      dirG = 0;
      dirB = COLOR_INCREMENT;
    }
    if((ledRValue <= MIN_THRESHOLD) && (ledGValue >= MAX_THRESHOLD) && (ledBValue >= MAX_THRESHOLD))
    {
      dirR = 0;
      dirG = -COLOR_INCREMENT;
      dirB = 0;
    }
    if((ledRValue <= MIN_THRESHOLD) && (ledGValue <= MIN_THRESHOLD) && (ledBValue >= MAX_THRESHOLD))
    {
      dirR = COLOR_INCREMENT;
      dirG = 0;
      dirB = 0;
    }
    if((ledRValue >= MAX_THRESHOLD) && (ledGValue <= MIN_THRESHOLD) && (ledBValue >= MAX_THRESHOLD))
    {
      dirR = 0;
      dirG = 0;
      dirB = -COLOR_INCREMENT;
    }
  }
  if(MODE_MUSIC == crtMode)
  {
     setColor(0, -1); 
     
     trebleTrigger = abs(trebleTrigger);

     if(trebleTrigger > 27)
     {
      setColor(trebleTrigger * 2, 1);
     }
     else if(trebleTrigger > 20 && midTrigger < 60)
     {
      setColor(trebleTrigger * 3, 1);
     }
     
     if(bassTrigger)
     {
      bassTimeout = 5;
      
      if(noBass == 1 && jumpTimeout == 1)
      {
        jumpTimeout = 0;
        
      }
      noBass = 0;
      setColor(0, 2);
     }
     else
     {
      if(bassTimeout == 0)
        noBass = 1;

      if(bassTimeout > 0)
        bassTimeout--;
      else
        bassTimeout = 0;
     }
  }
}
