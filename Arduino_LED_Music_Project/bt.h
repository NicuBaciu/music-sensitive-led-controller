#ifndef _BT_H
#define _BT_H

#define MAX_LEN_MSG 20

byte rxMessage[MAX_LEN_MSG];
byte rxMessageLen;

byte msgReceivedFlag; 

void startUpBT();

void receiveBT();

#endif // _BT_H
